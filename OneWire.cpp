#include <Arduino.h>
#include "OneWire.h"
#include "types.h"

DS2483 oneWireBridgeIC;

OneWire::OneWire(){}

void OneWire::init(){
	//Scan for all oneWire devices
	byte address[8]; //Temp buffer
  byte i;

  //Init variables
  board_info.tempProbeQty = 0;
  for (i = 0; i < 8; i++){
    address[i]=0;
    board_info.id[i]=0;
    board_info.tempProbes[0][i]=0;
    board_info.tempProbes[1][i]=0;
    board_info.tempProbes[2][i]=0;
  }
  
  oneWireBridgeIC.reset_search();
	while (oneWireBridgeIC.search(address)){
		Serial.print("OneWire Device found: 0x");
    for (i = 0; i < 8; i++){
        if(address[i]<16){
          Serial.print("0");
          Serial.print(address[i],HEX);
        }else{
          Serial.print(address[i],HEX);
        }
    }
    Serial.println("");
		//Process depending on family code
		switch (address[0]) {
		case 0x01:  //ID chip
			for (i = 0; i < 8; i++){
				board_info.id[i] = address[i];
			}
			board_info.id[0] = 0;
			break;
		case 0x28:
			//Serial.println("DS18B20 Temp. probe found!");
      //Copy temp probe address
      for (i = 0; i < 8; i++){
        board_info.tempProbes[board_info.tempProbeQty][i] = address[i];
      }
      board_info.tempProbeQty++;
			break;
		}
	}
}

//returns true when a new value is available
//must return as quick as possible. Called repeatedly
byte OneWire::readDS18B20(float *temp1, float *temp2, float *temp3){
  byte i;
  byte newData = 0;
  static byte converting = 0;
  static unsigned long conversion_start_time = 0;
  static float tempC[3] = {0.0,0.0,0.0};
  static byte step = 0;
  static byte probe_number = 0;
  byte presence;
  static byte result_byte_1 = 0;
  static byte result_byte_2 = 0;

  if(!converting){
    //start conversion for each probe
    if(probe_number < board_info.tempProbeQty){
      switch(step){
        case 0: //Reset
          if(oneWireBridgeIC.reset_poll(&presence)){
            step++;
          }
        break;
        case 1: //Select
          if(oneWireBridgeIC.select_poll(board_info.tempProbes[probe_number])){
            step++;
          }
        break;
        case 2: //Start conversion
          if(oneWireBridgeIC.write_poll(0x44)){
            step=0;
            probe_number ++;
          }
        break;
      }
    }else{
      probe_number = 0;
      converting = 1;
      conversion_start_time = micros();
    }
  }else{ //conversion in progress
    if(micros()-conversion_start_time > 800000){ //conversion takes 750ms
      //read result for each probe
      if(probe_number < board_info.tempProbeQty){
        switch(step){
          case 0: //Reset
            if(oneWireBridgeIC.reset_poll(&presence)){
              step ++;
            }
          break;
          case 1: //Select
            if(oneWireBridgeIC.select_poll(board_info.tempProbes[probe_number])){
              step ++;
            }
          break;
          case 2: //Start conversion
            if(oneWireBridgeIC.write_poll(0xBE)){ // Read Scratchpad
              step ++;
            }
          break;
          case 3: //Read byte 1
            if(oneWireBridgeIC.read_poll(&result_byte_1)){ // Read result part1
              step ++;
            }
          break;
          case 4: //Read byte 2
            if(oneWireBridgeIC.read_poll(&result_byte_2)){ // Read result part2
              step = 0;            
              int16_t rawTemperature = ((int16_t)result_byte_1) | (((int16_t)result_byte_2) << 8);
              tempC[probe_number] = (float)rawTemperature * 0.0625;
              probe_number ++;
            }
          break;
        }
      }else{
        probe_number = 0;
        converting = 0;
        newData = 1;
      }      
    }
  }

  //return latest readings
  *temp1 = tempC[0];
  *temp2 = tempC[1];
  *temp3 = tempC[2];
  return newData;
}

void OneWire::print(){}
