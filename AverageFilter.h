#ifndef AVERAGE_FILTER_h
#define AVERAGE_FILTER_h

#include <Arduino.h>

class AverageFilter
{
public:
	AverageFilter(); //constructor, simply sets total and counter to zero
	void newValue(float newValue); //increments counter by one, and adds newValue to total
	byte isData(); //returns result of (counter>0), ie returns the "flag" value
	float getAverage(); //called before sending the data through usb. Returns total/counter, resets counter and total to zero, and saves result into oldValue. If counter is zero, return oldValue instead.
private:
	float total; //sum of all samples accumulated with calls to newValue().
	float oldValue; //in case getAverage is called when counter is zero, return this old value (avoid dividing by zero). Counter will be zero sometimes for sensors that refresh slowly (ie load cells).
	int counter; //total number of samples accumulated into total variable. If value exceeds 10000, just call getAverage() and things will go back to zero correctly. Normally, when connected with USB, the counter will be reset before reaching a few hundred.
};

#endif