/*

Modified by Tyto Robotics Inc. to use the hardware DS2483 I2C to OneWire chip.
The code for software oneWire was excerpted and inspired by the oneWire library bearing this copyright:

//Copyright (c) 2007, Jim Studt  (original old version - many contributors since)
//
//The latest version of this library may be found at:
//  http://www.pjrc.com/teensy/td_libs_OneWire.html
//
//OneWire has been maintained by Paul Stoffregen (paul@pjrc.com) since
//January 2010.  At the time, it was in need of many bug fixes, but had
//been abandoned the original author (Jim Studt).  None of the known
//contributors were interested in maintaining OneWire.  Paul typically
//works on OneWire every 6 to 12 months.  Patches usually wait that
//long.  If anyone is interested in more actively maintaining OneWire,
//please contact Paul.
//
//Version 2.3:
//  Unknonw chip fallback mode, Roger Clark
//  Teensy-LC compatibility, Paul Stoffregen
//  Search bug fix, Love Nystrom
//
//Version 2.2:
//  Teensy 3.0 compatibility, Paul Stoffregen, paul@pjrc.com
//  Arduino Due compatibility, http://arduino.cc/forum/index.php?topic=141030
//  Fix DS18B20 example negative temperature
//  Fix DS18B20 example's low res modes, Ken Butcher
//  Improve reset timing, Mark Tillotson
//  Add const qualifiers, Bertrik Sikken
//  Add initial value input to crc16, Bertrik Sikken
//  Add target_search() function, Scott Roberts
//
//Version 2.1:
//  Arduino 1.0 compatibility, Paul Stoffregen
//  Improve temperature example, Paul Stoffregen
//  DS250x_PROM example, Guillermo Lovato
//  PIC32 (chipKit) compatibility, Jason Dangel, dangel.jason AT gmail.com
//  Improvements from Glenn Trewitt:
//  - crc16() now works
//  - check_crc16() does all of calculation/checking work.
//  - Added read_bytes() and write_bytes(), to reduce tedious loops.
//  - Added ds2408 example.
//  Delete very old, out-of-date readme file (info is here)
//
//Version 2.0: Modifications by Paul Stoffregen, January 2010:
//http://www.pjrc.com/teensy/td_libs_OneWire.html
//  Search fix from Robin James
//    http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1238032295/27#27
//  Use direct optimized I/O in all cases
//  Disable interrupts during timing critical sections
//    (this solves many random communication errors)
//  Disable interrupts during read-modify-write I/O
//  Reduce RAM consumption by eliminating unnecessary
//    variables and trimming many to 8 bits
//  Optimize both crc8 - table version moved to flash
//
//Modified to work with larger numbers of devices - avoids loop.
//Tested in Arduino 11 alpha with 12 sensors.
//26 Sept 2008 -- Robin James
//http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1238032295/27#27
//
//Updated to work with arduino-0008 and to include skip() as of
//2007/07/06. --RJL20
//
//Modified to calculate the 8-bit CRC directly, avoiding the need for
//the 256-byte lookup table to be loaded in RAM.  Tested in arduino-0010
//-- Tom Pollard, Jan 23, 2008
//
//Jim Studt's original library was modified by Josh Larios.
//
//Tom Pollard, pollard@alum.mit.edu, contributed around May 20, 2008
//
//Permission is hereby granted, free of charge, to any person obtaining
//a copy of this software and associated documentation files (the
//"Software"), to deal in the Software without restriction, including
//without limitation the rights to use, copy, modify, merge, publish,
//distribute, sublicense, and/or sell copies of the Software, and to
//permit persons to whom the Software is furnished to do so, subject to
//the following conditions:
//
//The above copyright notice and this permission notice shall be
//included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
//LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
//OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
//WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//Much of the code was inspired by Derek Yerger's code, though I don't
//think much of that remains.  In any event that was..
//    (copyleft) 2006 by Derek Yerger - Free to distribute freely.
//
//The CRC code was excerpted and inspired by the Dallas Semiconductor
//sample code bearing this copyright.
////---------------------------------------------------------------------------
//// Copyright (C) 2000 Dallas Semiconductor Corporation, All Rights Reserved.
////
//// Permission is hereby granted, free of charge, to any person obtaining a
//// copy of this software and associated documentation files (the "Software"),
//// to deal in the Software without restriction, including without limitation
//// the rights to use, copy, modify, merge, publish, distribute, sublicense,
//// and/or sell copies of the Software, and to permit persons to whom the
//// Software is furnished to do so, subject to the following conditions:
////
//// The above copyright notice and this permission notice shall be included
//// in all copies or substantial portions of the Software.
////
//// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//// MERCHANTABILITY,  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//// IN NO EVENT SHALL DALLAS SEMICONDUCTOR BE LIABLE FOR ANY CLAIM, DAMAGES
//// OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
//// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//// OTHER DEALINGS IN THE SOFTWARE.
////
//// Except as contained in this notice, the name of Dallas Semiconductor
//// shall not be used except as stated in the Dallas Semiconductor
//// Branding Policy.
////--------------------------------------------------------------------------

*/

#include <Arduino.h>
#include "I2C.h"
#include "DS2483.h"

DS2483::DS2483()
{reset_search();}

//Ensures the bus is idle before the next operation
void DS2483::wait_ds2483_idle(){
	while (!is_ds2483_idle());
}

//Returns idle state
byte DS2483::is_ds2483_idle(){
	//Read status register
	I2c.write(DS2483_address, DS2483_CMD_SET_READ_PTR, DS2483_REGISTER_STATUS);
	I2c.read(DS2483_address, 1);
	byte status = I2c.receive();
	return !(0x01 & status); //Idle bit
}

// Resets the I2C <-> DS2483 bridge IC

byte DS2483::reset_ds2483(void)
{	
  wait_ds2483_idle();
	return I2c.write(DS2483_address, DS2483_CMD_RST);
}

// Perform the DS2483 reset function.
byte DS2483::reset(){
  byte presence;
  while(!reset_poll(&presence));
  return presence;
}

// Called repeateadly, reset finished when returns 1
// precence 1 if a device asserted a presence pulse, 0 otherwise.
byte DS2483::reset_poll(byte * presence)
{
	static byte step = 0;
  byte result = 0;
  *presence = 0;
  
  switch(step) {
      case 0:
          if(is_ds2483_idle()){
            I2c.write(DS2483_address, DS2483_CMD_BUS_RST);
            step++;
          }
          break;
      case 1:
          if(is_ds2483_idle()){
            step=0;
            result = 1;
            I2c.read(DS2483_address, 1);
            byte status = I2c.receive();
            *presence = (0x01 & status >> 1); //Presence pulse bit
          }
          break;
  }
  return result;
}

// Write a bit.
void DS2483::write_bit(byte v)
{
	wait_ds2483_idle();
	I2c.write((uint8_t)DS2483_address, (uint8_t)DS2483_CMD_1W_WRITE_BIT, (uint8_t)v << 7);
}

// Read a bit.
byte DS2483::read_bit(void)
{
	while(!reset());
	write(ROM_READ);
	return read();
}

// Write a byte.
void DS2483::write(byte v) {
	while(!write_poll(v));
}
byte DS2483::write_poll(byte v) {
  if(is_ds2483_idle()){
    I2c.write((uint8_t)DS2483_address, (uint8_t)DS2483_CMD_1W_WRITE_BYTE, v);
    return true;
  }
  return false;
}

void DS2483::write_bytes(const byte *buf, uint16_t count) {
	for (uint16_t i = 0; i < count; i++)
		write(buf[i]);
}

//
// Read a byte
//
byte DS2483::read() {
  byte result;
  while(!read_poll(&result));
  return result;
} 
byte DS2483::read_poll(byte *result) {
  static byte step = 0;
  byte done = 0;
  *result = 0;
  
  switch(step) {
      case 0:
          if(is_ds2483_idle()){
            I2c.write(DS2483_address, DS2483_CMD_1W_READ_BYTE);
            step++;
          }
          break;
      case 1:
          if(is_ds2483_idle()){
            step=0;
            done = 1;
            I2c.write(DS2483_address, DS2483_CMD_SET_READ_PTR, DS2483_REGISTER_READ_DATA);
            I2c.read(DS2483_address, 1);
            *result = I2c.receive();
          }
          break;
  }
  return done;
}

void DS2483::read_bytes(byte *buf, uint16_t count) {
	for (uint16_t i = 0; i < count; i++)
		buf[i] = read();
}

// Prints the ROM assuming only one slave device is on the bus. Will not work for multiple devices.
// Return 1 if crc check passed (ie good data)
byte DS2483::readROM()
{
	reset();
	wait_ds2483_idle();
	I2c.write(DS2483_address, DS2483_CMD_1W_WRITE_BYTE, ROM_READ);
	byte i;
	Serial.print("DS2483 READ ROM: ");
	for (i = 0; i<8; i++){
		address[i] = read();
		Serial.print(address[i], HEX);
		Serial.print(" ");
	}
	byte result = crc8(address, 8);
	Serial.println();
	return result;
}

//
// Do a ROM select
//
void DS2483::select(const byte rom[8]){
  while(!select_poll(rom));
}
byte DS2483::select_poll(const byte rom[8])
{
	static byte i = 8;
  if(i == 8){
    if(write_poll(ROM_MATCH)){// Choose ROM
      i = 0;
      return false;
    };           
  }else{
    if(write_poll(rom[i])){
      i++;
      if(i==8)
        return true;
      else
        return false;
    };
  }
}

//
// Do a ROM skip
//
void DS2483::skip()
{
	write(ROM_SKIP);           // Skip ROM
}

#if DS2483_SEARCH

//Issue a triplet command, for rom search
//Returns the status register
byte DS2483::triplet(byte direction)
{
  wait_ds2483_idle();
  I2c.write((uint8_t)DS2483_address, (uint8_t)DS2483_CMD_TRIPLET, (uint8_t)direction * 0xFF);
  wait_ds2483_idle();
  //Read status register
  I2c.read(DS2483_address, 1);
  return I2c.receive();
}

//
// You need to use this function to start a search again from the beginning.
// You do not need to do it for the first search, though you could.
//
void DS2483::reset_search()
{
	// reset the search state
	LastDiscrepancy = -1;
  LastDeviceFlag = 0;
	for (int i = 7;; i--) {
		ROM_NO[i] = 0;
		if (i == 0) break;
	}
}

// Setup the search to find the device type 'family_code' on the next call
// to search(*newAddr) if it is present.
//
void DS2483::target_search(byte family_code)
{
	/*// set the search state to find SearchFamily type devices
	ROM_NO[0] = family_code;
	for (byte i = 1; i < 8; i++)
		ROM_NO[i] = 0;
	LastDiscrepancy = 64;
	LastFamilyDiscrepancy = 0;
	LastDeviceFlag = FALSE;*/
}

//
// Perform a search. If this function returns a '1' then it has
// enumerated the next device and you may retrieve the ROM from the
// DS2483::address variable. If there are no devices, no further
// devices, or something horrible happens in the middle of the
// enumeration then a 0 is returned.  If a new device is found then
// its address is copied to newAddr.  Use DS2483::reset_search() to
// start over.
//
// --- Replaced by the one from the Dallas Semiconductor web site ---
//--------------------------------------------------------------------------
// Perform the 1-Wire Search Algorithm on the 1-Wire bus using the existing
// search state.
// Return TRUE  : device found, ROM number in ROM_NO buffer
//        FALSE : device not found, end of search
//
byte DS2483::search(byte *newAddr)
{		
  //Performs the 1wire search algorithm https://www.maximintegrated.com/en/app-notes/index.mvp/id/187 with minor changes because of DS2483 triplet command function
  
	byte last_zero = -1;

  //Reset, assert presence, and check if more devices
  if(!reset() || LastDeviceFlag){
    reset_search();
    return false; 
  }
	
	// issue the search command
	write(ROM_SEARCH);

  // scan all 64 bits for next available device
  char i;
  for(i=0;i<64;i++){
    byte byteIndex = i/8;
    byte bitInByte = i%8;
    
    byte currentBit = bitRead(ROM_NO[byteIndex],bitInByte);

    //According to Table 3 of https://www.maximintegrated.com/en/app-notes/index.mvp/id/187
    byte searchDirection;
    if(i==LastDiscrepancy){
      searchDirection = 1;
    }else{
      if(i<LastDiscrepancy){
        searchDirection = currentBit;
      }else{
        searchDirection = 0;
      }
    }
    
    // perform a triplet search
    byte triplet_result = triplet(searchDirection);
    byte id_bit = bitRead(triplet_result,5);
    byte cmp_id_bit = bitRead(triplet_result,6);
    byte direction_taken = bitRead(triplet_result,7);  

    // save result
    bitWrite(ROM_NO[byteIndex],bitInByte,direction_taken);

    if ((id_bit == 1) && (cmp_id_bit == 1)){
      Serial.println("oneWire Error");
      reset_search();
      return false; 
    }else{
      if ((id_bit == 0) && (cmp_id_bit == 0)){
        if(direction_taken==0) last_zero = i;
      }
    }
  }

  LastDiscrepancy = last_zero;

  // copy address into user's array
  for (int i = 0; i < 8; i++){
    newAddr[i] = ROM_NO[i];
  }

  if(LastDiscrepancy == -1){
    LastDeviceFlag = 1;  
  }
  
  //Check crc
  if(crc8(ROM_NO, 8)){
    return true;
  }else{
    reset_search();
    return false; 
  }
}

#endif

#if DS2483_CRC
// The 1-Wire CRC scheme is described in Maxim Application Note 27:
// "Understanding and Using Cyclic Redundancy Checks with Maxim iButton Products"
//

#if DS2483_CRC8_TABLE
// This table comes from Dallas sample code where it is freely reusable,
// though Copyright (C) 2000 Dallas Semiconductor Corporation
static const byte PROGMEM dscrc_table[] = {
	0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
	157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
	35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
	190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
	70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
	219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
	101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
	248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
	140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
	17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
	175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
	50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
	202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
	87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
	233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
	116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53 };

//
// Compute a Dallas Semiconductor 8 bit CRC. These show up in the ROM
// and the registers.  (note: this might better be done without to
// table, it would probably be smaller and certainly fast enough
// compared to all those delayMicrosecond() calls.  But I got
// confused, so I use this table from the examples.)
//
byte DS2483::crc8(const byte *addr, byte len)
{
	byte crc = 0;

	while (len--) {
		crc = pgm_read_byte(dscrc_table + (crc ^ addr[len]));
	}

	if (crc){
		//Serial.print("CRC: OK. ");
	}
	else{
		Serial.println("OneWire CRC Failed. ");
	}

	return crc;
}
#else
//
// Compute a Dallas Semiconductor 8 bit CRC directly.
// this is much slower, but much smaller, than the lookup table.
//
byte DS2483::crc8(const byte *addr, byte len)
{
	byte crc = 0;

	while (len--) {
		byte inbyte = *addr++;
		for (byte i = 8; i; i--) {
			byte mix = (crc ^ inbyte) & 0x01;
			crc >>= 1;
			if (mix) crc ^= 0x8C;
			inbyte >>= 1;
		}
	}

	Serial.print("CRC: ");
	if (crc){
		Serial.println("OK");
	}
	else{
		Serial.println("Failed");
	}

	return crc;
}
#endif

#if DS2483_CRC16
bool DS2483::check_crc16(const byte* input, uint16_t len, const byte* inverted_crc, uint16_t crc)
{
	crc = ~crc16(input, len, crc);
	return (crc & 0xFF) == inverted_crc[0] && (crc >> 8) == inverted_crc[1];
}

uint16_t DS2483::crc16(const byte* input, uint16_t len, uint16_t crc)
{
	static const byte oddparity[16] =
	{ 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0 };

	for (uint16_t i = 0; i < len; i++) {
		// Even though we're just copying a byte from the input,
		// we'll be doing 16-bit computation with it.
		uint16_t cdata = input[i];
		cdata = (cdata ^ crc) & 0xff;
		crc >>= 8;

		if (oddparity[cdata & 0x0F] ^ oddparity[cdata >> 4])
			crc ^= 0xC001;

		cdata <<= 6;
		crc ^= cdata;
		cdata <<= 1;
		crc ^= cdata;
	}
	return crc;
}
#endif

#endif
