/*
  I2C.h - Library for common i2c functions.
  Created by Tyto Robotics Inc., March 16, 2015.
  Released into the public domain.
*/

#ifndef I2C_h
#define I2C_h

#include "Arduino.h"

void i2c_init();
void writeRegister(byte dev_address, byte reg, byte data);
void writeRegisters(byte dev_address, byte reg, byte *buffer, byte len);
byte readRegister(byte dev_address, byte reg);
void readRegisters(byte dev_address, byte reg, byte *buffer, byte len);

#endif