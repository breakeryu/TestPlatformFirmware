/*
  DS2483.cpp - Library for the DS2483 i2c <-> oneWire bridge IC.
  Created by Tyto Robotics Inc., March 16, 2015.
  Released into the public domain.
*/

#include "DS2483.h"
#include <Arduino.h>
#include "I2C.h"

DS2483::DS2483()
{
}

// INITIALIZATION
byte DS2483::init()
{
	byte c = readRegister(DS2483_I2C_ADDR,CONFIG);  // Read CONFIG register
	
	Serial.print("DS2483...");
    if( c==0x06 ){
      Serial.println("Status: 0K");
    }else{
      Serial.println("Status: PROBLEM");
    }
    return 1;
}