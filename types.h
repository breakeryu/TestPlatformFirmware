#ifndef TYPES_H_
#define TYPES_H_

#include "RC_SIGNALS.h"

extern int16_t debug[4];
extern byte pin_states[4];
extern unsigned long last_poll_report_timestamp;

//Error codes (see Error.c)
typedef enum { I2Cbus, AccNoDetect, NAU7802_Init, ISL28022_Init, ISL28022_Overflow } error_codes;

//Board info
typedef struct {
	uint8_t version;
	uint8_t id[8];
    uint8_t pSensorAvailable;
  uint8_t tempProbeQty;
  uint8_t tempProbes[3][8];
} board_info_t;
extern board_info_t board_info;

//Information about this firwmare
typedef struct {
	uint8_t com_protocol_version, major, minor;
} firmware_info_t;
extern firmware_info_t firmware_info;

//Control inputs from PC (for the basic edition)
typedef struct {
  uint16_t ESC_PWM; //PWM pulse sent to ESC (ms)
} basic_control_t;
extern basic_control_t basic_control;

//Additional control inputs from PC (for the pro edition)
typedef struct {
	uint16_t S1_PWM; //Servo 1 pwm
	uint16_t S2_PWM; //Servo 2 pwm
	uint16_t S3_PWM; //Servo 3 pwm
} pro_control_t;
extern pro_control_t pro_control;

//Basic edition polled sensors
typedef struct {
	float esc_voltage; //in V
	float esc_current; //in A
  float esc_power; //in W
	float load_cell_thrust, load_cell_left;
	float brushless_hz; //motor brushless_hz
  float optical_hz; //motor optical_hz
  float temp1, temp2, temp3; //in degC
	uint8_t	flag_esc_voltage : 1;
	uint8_t	flag_esc_current : 1;
  uint8_t flag_esc_power : 1;
	uint8_t	flag_load_cell_thrust : 1;
	uint8_t	flag_load_cell_left : 1;
	uint8_t	flag_brushless_hz : 1;
  uint8_t flag_optical_hz : 1;
  uint8_t flag_temp_probe : 1;
} basic_sensors_t;
extern basic_sensors_t basic_sensors;

//Pro edition polled sensors (additional to the basic edition)
typedef struct {
	int16_t accx, accy, accz, accVib; //Accelerometer readings
    int16_t pressure_P, pressure_T;
	float load_cell_right;
	uint8_t	flag_acc : 1;
	uint8_t	flag_load_cell_right : 1;
} pro_sensors_t;
extern pro_sensors_t pro_sensors;

//Ohmeter data
typedef enum { OHM_IDLE, OHM_READING, OHM_DONE_OK, OHM_DONE_LOW, OHM_DONE_HIGH } ohmmeter_statuses;
typedef struct {
	byte status;
	float ohmmeter_reading;
} ohmmeter_t;
extern ohmmeter_t ohmmeter;

//Sensor capabilities/resolution
typedef struct {
  uint8_t acc_bits_per_g; //Accelerometer
} capabilites_t;

//Abort limits (abnormal state conditions, experiment must be stopped completely)
typedef struct {
  uint8_t max_accx; //Accelerometer
} abort_t;

typedef struct  {
  uint8_t rc_type; // protocol type. See RC_SIGNAL.h
  uint8_t rc_flag_init_enable : 1; // set to 1 to trigger an update to the control board
  uint16_t rc_custom_frequency; // applies only to protocol 9, standard PWM with custom frequency between 50 and 500 Hz
} rc_cb_t;
extern rc_cb_t rc_cb;

typedef struct {
  uint8_t cb_flag_active : 1; // control board is ready (it is alive and detected)
  uint8_t current_rc_type; // get back what rc type the control board is at.
  uint8_t cb_protocol; // this is the protocol code version between the RCbenchmark thrust stand and the control board
  uint16_t cb_version; // this is the firmware version on the control board. See corresponding TAG in the git for the control board.
} rc_info_t;
extern rc_info_t rc_info;

/*
typedef struct {
	uint8_t Temperature;
	uint16_t Voltage;
	uint16_t Current;
	uint16_t Consumption;
	uint16_t eRPM;
	uint8_t CRC8;
} CB_Tml_t;
extern CB_Tml_t cb_tml; //Write only in the rc.readTml()
*/

#endif /* TYPES_H_ */
