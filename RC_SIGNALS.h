#ifndef RC_SIGNALS_h
#define RC_SIGNALS_h

#include <Arduino.h> 

#define STM32F1_I2C_ADDR 0x32

#define STM32F1_I2C_CMD  0xc0
#define STM32F1_I2C_DATA 0x40
#define STM32F1_I2C_NULL 0x00
#define STM32F1_I2C_READ 0x80

#define STM32F1_I2C_SET_RCTYPE 0xc1
#define STM32F1_I2C_SYS_DEINIT 0xc2
#define STM32F1_I2C_START      0xc3
#define STM32F1_I2C_STOP       0xc4
#define STM32F1_I2C_SET_CUTOFF_TIME 0xc5
#define STM32F1_I2C_SET_CUTOFF_STATE 0xc6
#define STM32F1_I2C_SET_DSHOT_FRQ 0xc7
#define STM32F1_I2C_SET_TML       0xc8
/*
#define STM32F1_I2C_SET_DSHOT1200   0x01
#define STM32F1_I2C_SET_DSHOT600    0x02
#define STM32F1_I2C_SET_DSHOT300    0x03
#define STM32F1_I2C_SET_DSHOT150    0x04
#define STM32F1_I2C_SET_STANDARD    0x05
#define STM32F1_I2C_SET_ONESHOT42   0x06
#define STM32F1_I2C_SET_ONESHOT125  0x07
#define STM32F1_I2C_SET_MULTISHOT   0x08
*/
#define STM32F1_I2C_READ_FIRMWARE_VERSION 0x81
#define STM32F1_I2C_READ_TML              0x82
#define STM32F1_I2C_READ_CB_PROTOCOL      0x83
#define STM32F1_I2C_READ_CB_RC_TYPE		  0X84


#define STM32F1_CB_PRPTOCOL        0x01

#define STM32F1_FIRMWARE_VERSION_H_0 0x00  //Dev version
#define STM32F1_FIRMWARE_VERSION_L_0 0x01

#define STM32F1_FIRMWARE_VERSION_H_1 0x01  //Formal version
#define STM32F1_FIRMWARE_VERSION_L_1 0x01 

#define MERGE_UINT16_T(A,B) (A << 8| B)

#define STM32F1_FIRMWARE_VERSION_0 (MERGE_UINT16_T(STM32F1_FIRMWARE_VERSION_H_0, STM32F1_FIRMWARE_VERSION_L_0))
#define STM32F1_FIRMWARE_VERSION_1 (MERGE_UINT16_T(STM32F1_FIRMWARE_VERSION_H_1, STM32F1_FIRMWARE_VERSION_L_1))


#define isRCPWM(RC_Type) (     (RC_Type == RC_Standard)   || \
                               (RC_Type == RC_OneShot42)  || \
                               (RC_Type == RC_OneShot125) || \
                               (RC_Type == RC_MultiShot)  )

#define isDShot(RC_Type) ((RC_Type == RC_DShot1200) || \
                          (RC_Type == RC_DShot600)  || \
                          (RC_Type == RC_DShot300)  || \
                          (RC_Type == RC_DShot150)  ) 

#define isRCType(RC_Type)	((RC_Type == RC_DShot1200) || \
            							(RC_Type == RC_DShot600)   || \
            							(RC_Type == RC_DShot300)  || \
            							(RC_Type == RC_DShot150)  || \
            							(RC_Type == RC_Standard)  || \
            							(RC_Type == RC_OneShot42) || \
            							(RC_Type == RC_OneShot125) || \
            							(RC_Type == RC_MultiShot)) )          
                          
typedef enum {
	ENABLE = true,
	DISABLE = false,
} FuntionState;

typedef enum {
  // DO NOT CHANGE THIS ENUM ORDER, OR THE GUI WILL ALSO NEED TO BE REPROGRAMMED.
  RC_DShot1200 = 0x01, // values 0-2047
  RC_DShot600  = 0x02, // values 0-2047
  RC_DShot300  = 0x03, // values 0-2047
  RC_DShot150  = 0x04, // values 0-2047
  RC_Standard  = 0x05, // standard PWM (700-2300us) at 50Hz.
  RC_OneShot42 = 0x06, // values 1000-2000
  RC_OneShot125 = 0x07, // values 1000-2000
  RC_MultiShot = 0x08, // values 60-300
  RC_STD_Custom = 0x09, // standard PWM (700-2300us) with custom frequency 50 to 5000HZ
} RC_SignalsTypeDef;

class RC_SIGNALS
{
public: 
	RC_SIGNALS(); // Constructor
	void init(RC_SignalsTypeDef rc_type);			//Init the CB when the 1580/1585 mainboard is connected
	void set(uint16_t value, uint8_t index);		//Set the pwm output value
	boolean isActive(uint8_t index);				//Check channel is active
	boolean isCBActive(void);
	void reset(void);								//Reset the CB
	//void setCBtype(RC_SignalsTypeDef type);			//Set the CB RC type
	void initCB(RC_SignalsTypeDef rc_type, uint16_t frq);
	//void readTml(void);
private:
	boolean ESCActive;
	boolean servo1Active;
	boolean servo2Active;
	boolean servo3Active;
	boolean STM32CBActive;
	RC_SignalsTypeDef RC_Type;
	void setCBtype(RC_SignalsTypeDef type, uint16_t frq);	//Set the RC_Type with the customized frq
	void setCBValue(uint16_t value, byte index);
	void set15xxValue(uint16_t value, byte index);
	uint16_t getCBVersion(void);    
	uint8_t getCBProtocol(void);
	void setCutoff(FuntionState state);
	void deinitCB(void);
	void sentStart(void);
	void sentStop(void);
	//void setTML(uint8_t index, bool enable);
	void error_handle(void);
	bool versionCheck(uint16_t firmware_version, uint8_t protocol_version);
};

#endif
