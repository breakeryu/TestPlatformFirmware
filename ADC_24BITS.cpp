#include <Arduino.h>
#include "ADC_24BITS.h"
#include "Error.h"

ADC_24BITS::ADC_24BITS(){
	_adc0_channel_lock = 0;
}

NAU7802 adc0_24bits(0); // 1520 1580 1585
NAU7802 adc1_24bits(1); // 1580 1585
NAU7802 adc2_24bits(2); // 1585

void ADC_24BITS::init(bool is1580, bool is1585){
  byte fail = 0;
  if (!adc0_24bits.init()){ 
    fail += 1;
  }
  if(is1580 || is1585){
    if (!adc1_24bits.init()){ 
      fail += 2;
    }    
  }
  if(is1585){
    if (!adc2_24bits.init()){ 
      fail += 4;
    }    
  }
  
  if(fail){
    Serial.print("ADC failure: ");
    Serial.println(fail);
    while(1){
      red_led(1);  
    }  
  }
}

//Call this function until it returns true (avoid busy waits)
byte ADC_24BITS::get_adc(byte adc_num, byte channel, float *value){
  switch (adc_num) {
      case 0:
        return adc0_24bits.get_adc_val(value, channel); 
      break;
      case 1:
        return adc1_24bits.get_adc_val(value, channel);
      break;
      case 2:
        return adc2_24bits.get_adc_val(value, channel);
      break;
  }
}

//Returns the winding resistance value
//Call this function until it returns true (avoid busy waits)
byte ADC_24BITS::get_ohms(byte is1585, float *value, byte *result_state){
	//Take a number of readings for averaging
	float voltage;
	static byte counter;
	static float voltage_sum;
	static byte step = 0;

	switch (step)
	{
	case 0:
		voltage_sum = 0.0;
		counter = 0;
		step++;
		break;
	case 1:
    byte res;
    if(is1585){
      res = adc2_24bits.get_adc_val(&voltage, 1);
    }else{
      res = adc0_24bits.get_adc_val(&voltage, 1);  
    }
		if (res){
      if(is1585){
        voltage_sum -= voltage;
      }else{
        voltage_sum += voltage;  
      }
			
			if (++counter == 100){
				voltage = 0.01 * voltage_sum;
				*result_state = 0;

				//Calculate resistance on ohmmeter. Layout is VCC-120-R-120-GND
				//(ie R voltage is measured by the ADC, and two precision (0.1%)
				//120Ohm resistors are placed on either side connected to power.
				voltage = -(240.0*voltage) / (voltage - 5.0);
				if (voltage > 235.0){
					*result_state = 2;
				}else{
					if (voltage < 0.005) *result_state = 1; //To small to measure accurately, because of trace resistance and other factors.
				}

				*value = voltage; //in ohms.
				step=0;
				return true;
			}
		}
		break;
	}
	return false;
}
