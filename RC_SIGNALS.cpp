#include <Arduino.h>
#include "RC_SIGNALS.h"
#include <Servo.h>
#include "types.h"
#include "I2C.h"

Servo esc_pwm;
Servo s1_pwm;
Servo s2_pwm;
Servo s3_pwm;

RC_SIGNALS::RC_SIGNALS() {
  this->ESCActive = false;
  this->servo1Active = false;
  this->servo2Active = false;
  this->servo3Active = false;
  this->STM32CBActive = false;
  }

//Init the RC signal. RC control board or the PWM output on the 1580/1585 mainboard
void RC_SIGNALS::init(RC_SignalsTypeDef rc_type) {
	uint16_t CBversion = getCBVersion();
	delay(1);
	uint8_t CBprotocol = getCBProtocol();

	if (versionCheck(CBversion, CBprotocol)) {
		Serial.println("CB found");
		Serial.print("CB firmware Version: ");
		Serial.println(CBversion);
		Serial.print("CB protocol: ");
		Serial.println(CBprotocol);
		this->STM32CBActive = true;
		initCB(rc_type, 0);

		rc_info.cb_flag_active = true;
		rc_info.current_rc_type = (uint8_t)rc_type;
		rc_info.cb_version = CBversion;
		rc_info.cb_protocol = CBprotocol;
    
		//rc_cb.rc_init_enable = false;
		//rc_cb.rc_type = RC_Standard;
		//Serial.println("CB Reset");
	}
	else {
		this->STM32CBActive = false;
	}

	//Set initial values
	basic_control.ESC_PWM = 0xffff; //Equal to 0
	pro_control.S1_PWM = 0xffff;
	pro_control.S2_PWM = 0xffff;
	pro_control.S3_PWM = 0xffff;

	//Detach all servos
	set(0xffff, 0);
	set(0xffff, 1);
	set(0xffff, 2);
	set(0xffff, 3);
}

void RC_SIGNALS::reset() {
	if (this->STM32CBActive == false) {
		basic_control.ESC_PWM = 0xffff;
		pro_control.S1_PWM = 0xffff;
		pro_control.S2_PWM = 0xffff;
		pro_control.S3_PWM = 0xffff;
	}

	//Detach all servos
	set(0xffff, 0);
	set(0xffff, 1);
	set(0xffff, 2);
	set(0xffff, 3);
}


boolean RC_SIGNALS::isActive(uint8_t index) {
	boolean output;
	switch (index) {
	case 0:
		output = this->ESCActive;
		break;
	case 1:
		output = this->servo1Active;
		break;
	case 2:
		output = this->servo2Active;
		break;
	case 3:
		output = this->servo3Active;
		break;
	}
	return output;
}

boolean RC_SIGNALS::isCBActive(void) {
	return this->STM32CBActive;
}

//0 - ESC - physical pin 1  - PD3 -> 3
//1 - S1  - physical pin 2  - PD4 -> 4
//2 - S2  - physical pin 9  - PD5 -> 5
//3 - S3  - physical pin 12 - PB0 -> 8
void RC_SIGNALS::set(uint16_t value, uint8_t index) {

	if (this->STM32CBActive == true) {
	setCBValue(value, index);
	}
	else {
	set15xxValue(value, index);
	}
}

//Initialize the RC control board
void RC_SIGNALS::initCB(RC_SignalsTypeDef rc_type, uint16_t frq) {
	sentStop();
	delayMicroseconds(1000);  //Wait 1ms
	deinitCB();               //DeInit the CB
	delayMicroseconds(1000);  //Wait 1ms
	setCBtype(rc_type, frq);      //Initial the RC output type setting
	setCutoff(ENABLE);          //Enable the timeout cutoff
	//setTML(1);               //enable channel 1 TML
	sentStart();             //Sent Start Signal
	delayMicroseconds(1000); //Wait 1ms
}

/*
void RC_SIGNALS::setTML(uint8_t index, bool enable) {
	uint8_t temp[2];

	if(index<=4 && index!=0){
		temp[0] = STM32F1_I2C_SET_TML;
		temp[1] = enable ? 0x80 >> (index - 1) : 0x00 >> (index - 1);
		I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_CMD, temp, 2);
	}
	else return;
}
*/

void RC_SIGNALS::deinitCB() {
  uint8_t error;
  error = I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_CMD, STM32F1_I2C_SYS_DEINIT);

  if(error != 0) error_handle();
  else rc_info.cb_flag_active = true;
}

void RC_SIGNALS::sentStart() {
  uint8_t error;
  error = I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_CMD, STM32F1_I2C_START);  //Start the control board

  if(error != 0) error_handle();
  else rc_info.cb_flag_active = true;
}

void RC_SIGNALS::sentStop() {
  uint8_t error;
  I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_CMD, STM32F1_I2C_STOP);  //Stop

  if(error != 0) error_handle();
  else rc_info.cb_flag_active = true;
}

//Set the Control Board RC output type
/*
void RC_SIGNALS::setCBtype(RC_SignalsTypeDef type) {
  uint8_t temp[2];
  temp[0] = STM32F1_I2C_SET_RCTYPE;
  temp[1] = type;
  this->RC_Type = type;
  I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_CMD, temp, 2);
}
*/
//Set the Control Board RC output type with customized frq
void RC_SIGNALS::setCBtype(RC_SignalsTypeDef type, uint16_t frq) {
	uint8_t temp[4], error;

	if(type == RC_STD_Custom && frq <= 500 && frq >= 50){
		temp[0] = STM32F1_I2C_SET_RCTYPE;
		temp[1] = (uint8_t)type;
		temp[2] = (uint8_t)(frq >> 8);
		temp[3] = (uint8_t)frq;
		this->RC_Type = type;
    rc_info.current_rc_type = (uint8_t)type;
		error = I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_CMD, temp, 4);

    if(error != 0) error_handle();
    else rc_info.cb_flag_active = true;
	}
	else if((uint8_t)type >= 0x01 && (uint8_t)type <= 0x08){
		temp[0] = STM32F1_I2C_SET_RCTYPE;
		temp[1] = (uint8_t)type;
		this->RC_Type = type;
     rc_info.current_rc_type = (uint8_t)type;
		error = I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_CMD, temp, 2);

    if(error != 0) error_handle();
    else rc_info.cb_flag_active = true;
	}
}

//Set the Timeout cutoff
void RC_SIGNALS::setCutoff(FuntionState state) {
  uint8_t temp[2], error;
  temp[0] = STM32F1_I2C_SET_CUTOFF_STATE;

  if (state) temp[1] = 0xaa;
  else temp[1] = 0;

  error = I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_CMD, temp, 2);

  if(error != 0) error_handle();
  else rc_info.cb_flag_active = true;
}

//Return the Control board firmware version
uint16_t RC_SIGNALS::getCBVersion() {
  uint8_t temp[2], error;
  uint16_t version;

  error = I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_READ, STM32F1_I2C_READ_FIRMWARE_VERSION);
  
  if(error != 0) error_handle();
  else rc_info.cb_flag_active = true;

  I2c.read(STM32F1_I2C_ADDR, 2, temp);

  version |= temp[0] << 8;
  version |= temp[1];

  return version;
}

//Read the CB protocol
uint8_t RC_SIGNALS::getCBProtocol() {
	uint8_t temp[2], error;

  error = I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_READ, STM32F1_I2C_READ_CB_PROTOCOL);
  
  if(error != 0) error_handle();
  else rc_info.cb_flag_active = true;

	I2c.read(STM32F1_I2C_ADDR, 1, temp);

	return temp[0];
}

//Set the signal output on the S1580 PCB
void RC_SIGNALS::set15xxValue(uint16_t value, byte index) {

  if (value != 0xffff) {

    value = value > 2300 ? 2300 : value;
    value = value < 700 ? 700 : value;

    switch (index) {
      case 0:
        esc_pwm.write(value);
        esc_pwm.attach(3);
        this->ESCActive = true;
        break;
      case 1:
        s1_pwm.write(value);
        s1_pwm.attach(4);
        this->servo1Active = true;
        break;
      case 2:
        s2_pwm.write(value);
        s2_pwm.attach(5);
        this->servo2Active = true;
        break;
      case 3:
        s3_pwm.write(value);
        s3_pwm.attach(8);
        this->servo3Active = true;
        break;
    }
  }
  else {
    switch (index) {
      case 0:
        esc_pwm.detach();
        this->ESCActive = false;
        break;
      case 1:
        s1_pwm.detach();
        this->servo1Active = false;
        pinMode(4, INPUT_PULLUP);
        break;
      case 2:
        s2_pwm.detach();
        this->servo2Active = false;
        pinMode(5, INPUT_PULLUP);
        break;
      case 3:
        s3_pwm.detach();
        this->servo3Active = false;
        pinMode(8, INPUT_PULLUP);
        break;
    }
  }
}
//Set the signal value on the Control board
void RC_SIGNALS::setCBValue(uint16_t value, byte index) {
  uint8_t valueTemp[4];

  valueTemp[0] = (uint8_t)index;
  valueTemp[1] = (uint8_t)(value >> 8); //MSByte
  valueTemp[2] = (uint8_t)value;      //LSByte
  //Serial.println(value);
  uint8_t err = I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_DATA, valueTemp, 3);

  if(err != 0) error_handle();
  else rc_info.cb_flag_active = true;
  /*
  if (value != 0xffff) {
    switch (index) {
      case 0:
        this->ESCActive = true;
        break;
      case 1:
        this->servo1Active = true;
        break;
      case 2:
        this->servo2Active = true;
        break;
      case 3:
        this->servo3Active = true;
        break;
      default:
        break;
    }
  }

  else {
    switch (index) {
      case 0:
        this->ESCActive = false;
        break;
      case 1:
        this->servo1Active = false;
        break;
      case 2:
        this->servo2Active = false;
        break;
      case 3:
        this->servo3Active = false;
        break;
      default:
        break;
    }
  }
  */
}

void RC_SIGNALS::error_handle(){
  rc_info.cb_flag_active = false;
  this->STM32CBActive == false;
}

bool RC_SIGNALS::versionCheck(uint16_t firmware_version, uint8_t protocol_version) {
	
	return (firmware_version == STM32F1_FIRMWARE_VERSION_0 || firmware_version == STM32F1_FIRMWARE_VERSION_1) &&
		(protocol_version == STM32F1_CB_PRPTOCOL);
}

//Read the Telemetry from the control board

/*
void RC_SIGNALS::readTml() {
	uint8_t temp[10], error;

	error = I2c.write(STM32F1_I2C_ADDR, STM32F1_I2C_READ, STM32F1_I2C_READ_TML);
	if (error != 0) return;
	I2c.read(STM32F1_I2C_ADDR, 10, temp);

	cb_tml.Temperature = temp[0];
	cb_tml.Voltage = temp[1] << 8 | temp[2];
	cb_tml.Current = temp[3] << 8 | temp[4];
	cb_tml.Consumption = temp[5] << 8 | temp[6];
	cb_tml.eRPM = temp[7] << 8 | temp[8];
	cb_tml.Consumption = temp[9];
}
*/
