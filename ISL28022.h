#ifndef ISL28022_h
#define ISL28022_h

#include <Arduino.h>

#define ISL28022_I2C_Address 0x40

////////////////////////////////
// ISL28022 Class Declaration //
////////////////////////////////
class ISL28022
{
public:	
  ISL28022(); // Constructor
	byte init();
	float voltage(void); //Returns bus voltage in V
	float current(void); //Returns current
  float power(void); // Returns the power
  int16_t shuntVoltage(void); //Returns shunt voltage in 10uV steps, signed 
private:
  

enum ISL28022_REGISTERS {
	CONFIG = 0x00,
	SHUNT_V = 0x01,
	BUS_V = 0x02,
	POWER = 0x03,
	CURRENT = 0x04,
	CALIB = 0x05,
	SHUNT_V_THRESHOLD = 0x06,
	BUS_V_THRESHOLD = 0x07,
	DCS_INT_STATUS = 0x08,
	AUX_CONTROL = 0x09
};

enum ISL28022_CONFIG_BITS {
	RST = 15,
	BRNG1 = 14,
	BRNG0 = 13,
	PG1 = 12,
	PG0 = 11,
	BADC3 = 10,
	BADC2 = 9,
	BADC1 = 8,
	BADC0 = 7,
	SADC3 = 6,
	SADC2 = 5,
	SADC1 = 4,
	SADC0 = 3,
	MODE2 = 2,
	MODE1 = 1,
	MODE0 = 0
};

};

#endif
